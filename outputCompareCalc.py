import argparse

class InputError(Exception):
    pass

'''
@brief Gets the value of the output compare register and prescaler based on clock frequency and desired frequency.
@param fclk: Clock frequency of the microcontroller.
@param fDesired: The desired timer compare frequency. 
'''
def getOutputCompareReg(fclk, fDesired):
    # make fclk MHz
    fclk = fclk*10**6

    availablePrescalers = (1.0, 8.0, 64.0, 256.0, 1024.0)

    # Make sure that frequency is physically realisable.
    minFreq = (fclk *1.0) / (2*availablePrescalers[0]*2**16 *1.0)
    if fDesired < minFreq:
        raise InputError('Input error -- Desired frequency too small')


    # Find the lowest prescaler that allows for the desired output frequency.
    for N in availablePrescalers[::-1]:
        # Calculate the OCRnA value for that frequency
        OCRnA = (fclk *1.0)/(2*N*fDesired *1.0)
        if OCRnA < 2**16 and OCRnA > 2**14: 
            break

    return (int(N), int(OCRnA))

parser = argparse.ArgumentParser(description='A command line tool for calculating the prescaler and OCRnA register values for a 16bit AVR timer in CRC mode.')
parser.add_argument(dest='fclk', help='Clock frequency in MHz', type=int)
parser.add_argument(dest='fDesired', help='Desired frequency in Hz', type=int)

args = parser.parse_args()

try:
    (N, OCRnA) = getOutputCompareReg(args.fclk, args.fDesired)
except Exception as e:
    print 'Couldn\'t calculate register values -- {}'.format(str(e))
    exit()

print 'N -- {}'.format(N)
print 'OCRnA -- {} == 0x{:04x}'.format(OCRnA, OCRnA)
